class CreateLandlords < ActiveRecord::Migration[6.0]
  def change
    create_table :landlords do |t|
      t.string :first_name, limit: 255, null: false
      t.string :last_name, limit: 255, null: false
      t.string :email, limit: 191, null: false

      t.timestamps null: false
    end
    add_index :landlords, :email, unique: true
  end
end
