class AddNotNullToLandlordAndProperty < ActiveRecord::Migration[6.0]
  def change
    change_column_null :landlords_properties, :landlord_id, false
    change_column_null :landlords_properties, :property_id, false
  end
end
