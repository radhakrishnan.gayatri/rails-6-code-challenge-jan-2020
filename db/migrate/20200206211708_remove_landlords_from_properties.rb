class RemoveLandlordsFromProperties < ActiveRecord::Migration[6.0]
  def change
    remove_column :properties, :landlord_first_name, :string
    remove_column :properties, :landlord_last_name, :string
    remove_column :properties, :landlord_email, :string
  end
end
