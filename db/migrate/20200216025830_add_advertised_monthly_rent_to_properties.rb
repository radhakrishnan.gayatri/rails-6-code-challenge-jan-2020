class AddAdvertisedMonthlyRentToProperties < ActiveRecord::Migration[6.0]
  def change
  	add_column :properties, :advertised_monthly_rent, :decimal, precision: 8, scale: 2
  end
end
