class CreateTenanciesTenants < ActiveRecord::Migration[6.0]
  def change
    create_table :tenancies_tenants do |t|
      t.references :tenancy, index: true
      t.references :tenant, index: true

      t.timestamps null: false
    end
  end
end
