class AddNotNullToTenancyAndTenant < ActiveRecord::Migration[6.0]
  def change
    change_column_null :tenancies_tenants, :tenancy_id, false
    change_column_null :tenancies_tenants, :tenant_id, false
  end
end
