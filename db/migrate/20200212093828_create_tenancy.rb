class CreateTenancy < ActiveRecord::Migration[6.0]
  def change
    create_table :tenancies do |t|
      t.references :property, index: {unique: true}
      t.decimal :security_deposit, precision: 8, scale: 2, null: false
      t.decimal :monthly_rent, precision: 8, scale: 2, null: false
      t.date :start_date, null: false

      t.timestamps null: false
    end
  end
end
