class CreateLandlordsProperties < ActiveRecord::Migration[6.0]
  def change
    create_table :landlords_properties do |t|
      t.references :property, index: true
      t.references :landlord, index: true

      t.timestamps null: false
    end
  end
end
