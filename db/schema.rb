# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_17_215414) do

  create_table "landlords", force: :cascade do |t|
    t.string "first_name", limit: 255, null: false
    t.string "last_name", limit: 255, null: false
    t.string "email", limit: 191, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_landlords_on_email", unique: true
  end

  create_table "landlords_properties", force: :cascade do |t|
    t.integer "property_id", null: false
    t.integer "landlord_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["landlord_id"], name: "index_landlords_properties_on_landlord_id"
    t.index ["property_id"], name: "index_landlords_properties_on_property_id"
  end

  create_table "properties", force: :cascade do |t|
    t.string "property_name", null: false
    t.string "property_address", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.decimal "advertised_monthly_rent", precision: 8, scale: 2
    t.index ["property_name"], name: "index_properties_on_property_name", unique: true
  end

  create_table "tenancies", force: :cascade do |t|
    t.integer "property_id"
    t.decimal "security_deposit", precision: 8, scale: 2, null: false
    t.decimal "monthly_rent", precision: 8, scale: 2, null: false
    t.date "start_date", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["property_id"], name: "index_tenancies_on_property_id", unique: true
  end

  create_table "tenancies_tenants", force: :cascade do |t|
    t.integer "tenancy_id", null: false
    t.integer "tenant_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["tenancy_id"], name: "index_tenancies_tenants_on_tenancy_id"
    t.index ["tenant_id"], name: "index_tenancies_tenants_on_tenant_id"
  end

  create_table "tenants", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["username"], name: "index_users_on_username"
  end

end
