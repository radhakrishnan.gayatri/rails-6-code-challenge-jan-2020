# frozen_string_literal: true

FactoryBot.define do
  factory :property do
    sequence(:property_name) { |i| "Some Property Name #{i}" }
    property_address { 'Some Property Address' }
    advertised_monthly_rent { 2000.00 }
    landlords { |a| [a.association(:landlord)] }
  end
end

FactoryBot.define do
  factory :landlords_property do
    landlord
    property
  end
end