FactoryBot.define do
  factory :tenancy do
    property
    monthly_rent { 1000.75 }
    security_deposit { 2000.00 }
    start_date { Date.today }
    tenants { |a| [a.association(:tenant)] }
  end
end

FactoryBot.define do
  factory :tenancies_tenant do
    tenancy
    tenant
  end
end