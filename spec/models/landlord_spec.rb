# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Landlord, type: :model do
	describe 'presence of' do
    it 'first_name' do
      expect(
        build(:landlord, first_name: nil).errors_on(:first_name)
      ).to eq [I18n.t('errors.messages.blank')]
    end

    it 'last_name' do
      expect(
        build(:landlord, last_name: nil).errors_on(:last_name)
      ).to eq [I18n.t('errors.messages.blank')]
    end

    it 'email' do
      expect(
        build(:landlord, email: nil).errors_on(:email)
      ).to eq [I18n.t('errors.messages.blank')]
    end
  end

  describe 'format of' do
    it 'landlord_email' do
      expect(
        build(:landlord, email: 'somelandlordemail').errors_on(:email)
      ).to eq [I18n.t('errors.messages.invalid')]

      expect(
        build(:landlord, email: 'topfloor.ie').errors_on(:email)
      ).to eq [I18n.t('errors.messages.invalid')]

      expect(
        build(:landlord, email: 'somelandlordemail @topfloor.ie').errors_on(:email)
      ).to eq [I18n.t('errors.messages.invalid')]

      expect(
        build(:landlord, email: 'somelandlordemail@topfloor').errors_on(:email)
      ).to eq [I18n.t('errors.messages.invalid')]

      expect(
        build(:landlord, email: 'somelandlordemail@topfloor.ie')
      ).to be_valid

      expect(
        build(:landlord, email: 'somelandlordemail@topfloor.co.uk')
      ).to be_valid
    end
  end

  describe 'length of' do
    it 'landlord_first_name having max of Landlord::FIRST_NAME_MAX_LENGTH' do
      expect(
        build(:landlord,
              first_name: 'a' * (Landlord::FIRST_NAME_MAX_LENGTH + 1)).errors_on(:first_name)
      ).to eq [I18n.t(
        'errors.messages.too_long',
        count: Landlord::FIRST_NAME_MAX_LENGTH
      )]

      expect(
        build(:landlord, first_name: 'a' * Landlord::FIRST_NAME_MAX_LENGTH)
      ).to be_valid
    end

    it 'landlord_last_name having max of Landlord::LAST_NAME_MAX_LENGTH' do
      expect(
        build(:landlord,
              last_name: 'a' * (Landlord::LAST_NAME_MAX_LENGTH + 1)).errors_on(:last_name)
      ).to eq [I18n.t(
        'errors.messages.too_long',
        count: Landlord::LAST_NAME_MAX_LENGTH
      )]

      expect(
        build(:landlord, last_name: 'a' * Landlord::LAST_NAME_MAX_LENGTH)
      ).to be_valid
    end

    it 'landlord_email having max of Landlord::EMAIL_MAX_LENGTH' do
      email_domain_substring = '@topfloor.ie'
      max_name_length = Landlord::EMAIL_MAX_LENGTH  - email_domain_substring.length
      valid_landlord_email = "#{'a' * max_name_length}#{email_domain_substring}"

      expect(
        build(:landlord, email: "a#{valid_landlord_email}").errors_on(:email)
      ).to eq [I18n.t(
        'errors.messages.too_long',
        count: Landlord::EMAIL_MAX_LENGTH
      )]

      expect(
        build(:landlord, email: valid_landlord_email)
      ).to be_valid
    end
  end

  describe 'destroy' do
    let (:landlord) { create(:landlord) }
    it 'triggers callback' do
      expect(landlord).to receive(:destroy_related_properties)
      expect(landlord.destroy)
    end

    it 'destroys associated properties' do
      landlord = build(:landlord, first_name: 'abc', last_name: 'xyz', email: 'abc@xyz.com')
      property = build(:property, property_name: 'Apple', property_address: 'Elm')
      property.landlords << landlord

      expect(landlord.destroy).to eq landlord
      expect(landlord.id).to eq nil
      expect(property.landlords_properties).not_to exist
      expect(property.id).to eq nil
    end
  end
end