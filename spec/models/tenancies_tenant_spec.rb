# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TenanciesTenant, type: :model do
  describe 'presence of' do
    it 'tenancy' do
      expect(
        build(:tenancies_tenant, tenancy: nil).errors_on(:tenancy)
      ).to eq [I18n.t('errors.messages.blank'), I18n.t('errors.messages.must_exist')]
    end

    it 'tenant' do
      expect(
        build(:tenancies_tenant, tenant: nil).errors_on(:tenant)
      ).to eq [I18n.t('errors.messages.blank'), I18n.t('errors.messages.must_exist')]
    end
  end

  describe 'uniqueness of' do
    it 'tenancy tenant record' do
      tenancy_tenant = create(:tenancies_tenant)
      expect{
        create(:tenancies_tenant,
          tenancy_id: tenancy_tenant.tenancy_id,
          tenant_id: tenancy_tenant.tenant_id
        )
      }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end