# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Tenancy, type: :model do
  describe 'presence of' do
  	it 'property' do
      expect(
        build(:tenancy, property: nil).errors_on(:property)
      ).to eq [I18n.t('errors.messages.blank'),I18n.t('errors.messages.must_exist')]
    end

    it 'start_date' do
      expect(
        build(:tenancy, start_date: nil).errors_on(:start_date)
      ).to eq [I18n.t('errors.messages.blank')]
    end

  	it 'security_deposit' do
      expect(
        build(:tenancy, security_deposit: nil).errors_on(:security_deposit)
      ).to eq [I18n.t('errors.messages.not_a_number'), I18n.t('errors.messages.blank')]
  	end

		it 'monthly_rent' do
      expect(
        build(:tenancy, monthly_rent: nil).errors_on(:monthly_rent)
      ).to eq [I18n.t('errors.messages.not_a_number'), I18n.t('errors.messages.blank')]
  	end

  	it 'at least one tenant' do
  		expect(
        build(:tenancy,
          property_id: create(:property).id,
          security_deposit: 2500.00,
          monthly_rent: 1000.00,
          start_date: Date.today,
          tenants: []).errors_on(:tenants)
      ).to eq [I18n.t('errors.messages.at_least_one_tenant')]
		end
  end

  describe 'should have' do
  	it 'security_deposit greater than 0.0' do
  		expect(
        build(:tenancy,
          property_id: create(:property).id,
          security_deposit: -1,
          monthly_rent: 1000.00,
          start_date: Date.today
        ).errors_on(:security_deposit)
      ).to eq ["must be greater than 0.0"]
  	end

  	it 'monthly_rent than 0.0' do
  		expect(
        build(:tenancy,
          property_id: create(:property).id,
          security_deposit: 1000.00,
          monthly_rent: -1,
          start_date: Date.today
        ).errors_on(:monthly_rent)
      ).to eq ["must be greater than 0.0"]
  	end
  end
end