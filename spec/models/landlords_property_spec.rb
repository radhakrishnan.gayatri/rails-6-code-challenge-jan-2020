# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LandlordsProperty, type: :model do
  describe 'presence of' do
    it 'lanldord' do
      expect(
        build(:landlords_property, landlord: nil).errors_on(:landlord)
      ).to eq [I18n.t('errors.messages.blank'), I18n.t('errors.messages.must_exist')]
    end

    it 'property' do
      expect(
        build(:landlords_property, property: nil).errors_on(:property)
      ).to eq [I18n.t('errors.messages.blank'), I18n.t('errors.messages.must_exist')]
    end
  end

  describe 'uniqueness of' do
    it 'landlords property record' do
      landlords_property = create(:landlords_property)
      expect{
        create(:landlords_property,
          landlord_id: landlords_property.landlord_id,
          property_id: landlords_property.property_id
        )
      }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end