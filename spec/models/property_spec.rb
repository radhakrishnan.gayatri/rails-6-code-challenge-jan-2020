# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Property, type: :model do
  let (:landlord1) { create(:landlord, first_name: 'abc', last_name: 'xyz', email: 'abc@xyz.com') }

  describe 'presence of' do
    it 'property_name' do
      expect(
        build(:property, property_name: nil).errors_on(:property_name)
      ).to eq [I18n.t('errors.messages.blank')]
    end

    it 'property_address' do
      expect(
        build(:property, property_address: nil).errors_on(:property_address)
      ).to eq [I18n.t('errors.messages.blank')]
    end

    it 'at least one landlord' do
      expect(
        build(:property,
          property_name: 'abc',
          property_address: 'def',
          landlords: []).errors_on(:landlords)
      ).to eq [I18n.t('errors.messages.at_least_one_landlord')]
    end
  end

  describe 'uniqueness of' do
    let(:property1) { create(:property, property_name: 'SoMePrOpErTyNaMe') }

    it 'property_name regardless of case' do
      expect(
        build(:property, property_name: property1.property_name).errors_on(:property_name)
      ).to eq [I18n.t('errors.messages.taken')]

      expect(
        build(:property, property_name: property1.property_name.upcase).errors_on(:property_name)
      ).to eq [I18n.t('errors.messages.taken')]

      expect(
        build(:property, property_name: property1.property_name.downcase).errors_on(:property_name)
      ).to eq [I18n.t('errors.messages.taken')]
    end
  end

  describe 'length of' do
    it 'property_name having max of Property::PROPERTY_NAME_MAX_LENGTH' do
      expect(
        build(:property, property_name: 'a' * (Property::PROPERTY_NAME_MAX_LENGTH + 1)).errors_on(:property_name)
      ).to eq [I18n.t(
        'errors.messages.too_long',
        count: Property::PROPERTY_NAME_MAX_LENGTH
      )]

      expect(
        build(:property, property_name: 'a' * Property::PROPERTY_NAME_MAX_LENGTH)
      ).to be_valid
    end

    it 'property_address having max of Property::PROPERTY_ADDRESS_MAX_LENGTH' do
      expect(
        build(:property,
              property_address: 'a' * (Property::PROPERTY_ADDRESS_MAX_LENGTH + 1)).errors_on(:property_address)
      ).to eq [I18n.t(
        'errors.messages.too_long',
        count: Property::PROPERTY_ADDRESS_MAX_LENGTH
      )]

      expect(
        build(:property, property_address: 'a' * Property::PROPERTY_ADDRESS_MAX_LENGTH)
      ).to be_valid
    end
  end
end
