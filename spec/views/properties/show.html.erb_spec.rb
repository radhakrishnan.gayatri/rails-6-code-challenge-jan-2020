# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'properties/show', type: :view do
  it 'displays the Property' do
    property = create(:property)
    assign(:property, property)

    render

    expect(rendered).to include 'Property'

    expect(rendered).to include property.property_name
    expect(rendered).to include property.property_address
    expect(rendered).to include property.advertised_monthly_rent.to_s
    expect(rendered).to include property.landlords[0].first_name
    expect(rendered).to include property.landlords[0].last_name
    expect(rendered).to include property.landlords[0].email
  end
end
