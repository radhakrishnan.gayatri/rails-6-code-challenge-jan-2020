# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'tenancies/new', type: :view do
  context 'without validation errors' do
    it 'displays new Tenancy form' do
      assign(:tenancy, Tenancy.new)

      render

      expect(rendered).to include 'New Tenancy'

      expect(rendered).to match have_css 'form[action="/tenancies"] select[name="tenancy[property_id]"]'
      expect(rendered).to match have_css 'form[action="/tenancies"] select[name="tenancy[tenant_ids][]"]'

      expect(rendered).to match have_css 'form[action="/tenancies"] input[name="tenancy[security_deposit]"]'
      expect(rendered).to match have_css 'form[action="/tenancies"] input[name="tenancy[monthly_rent]"]'

      expect(rendered).to match have_css 'form[action="/tenancies"] input[name="tenancy[start_date]"]'
      expect(rendered).to match have_css 'form[action="/tenancies"] button[type="submit"]'

      expect(rendered).to_not match(/error/) # no errors on page
    end
  end

  context 'with validation errors' do
    it 'displays new Tenancy form with error messages' do
      tenancy = build(:tenancy, security_deposit: nil)
      tenancy.validate

      assign(:tenancy, tenancy)

      render

      expect(rendered).to include 'New Tenancy'

      expect(rendered).to match have_css 'form[action="/tenancies"] select[name="tenancy[property_id]"]'
      expect(rendered).to match have_css 'form[action="/tenancies"] select[name="tenancy[tenant_ids][]"]'

      expect(rendered).to match have_css 'form[action="/tenancies"] input[name="tenancy[security_deposit]"]'
      expect(rendered).to match have_css 'form[action="/tenancies"] input[name="tenancy[monthly_rent]"]'

      expect(rendered).to match have_css 'form[action="/tenancies"] input[name="tenancy[start_date]"]'
      expect(rendered).to match have_css 'form[action="/tenancies"] button[type="submit"]'

      expect(rendered).to match(/Is not a number/)
      expect(rendered).to match(/can&#39;t be blank/)
    end
  end
end
