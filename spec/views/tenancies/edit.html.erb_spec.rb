# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'tenancies/edit', type: :view do
  context 'without validation errors' do
    it 'displays edit Tenancy form' do
      tenancy = create(:tenancy)
      assign(:tenancy, tenancy)

      render

      expect(rendered).to include 'Edit'

      expect(rendered).to match(
        have_css("form[action=\"/tenancies/#{tenancy.id}\"] select[name=\"tenancy[property_id]\"]")
      )

      expect(rendered).to have_field('tenancy_property_id', disabled: true)

      expect(rendered).to match(
        have_css("form[action=\"/tenancies/#{tenancy.id}\"] select[name=\"tenancy[tenant_ids][]\"]")
      )

      expect(rendered).to match(
        have_css("form[action=\"/tenancies/#{tenancy.id}\"] input[name=\"tenancy[start_date]\"]")
      )

      expect(rendered).to have_field('tenancy_start_date', disabled: true)

      expect(rendered).to match(
        have_css("form[action=\"/tenancies/#{tenancy.id}\"] input[name=\"tenancy[security_deposit]\"]")
      )

      expect(rendered).to match(
        have_css("form[action=\"/tenancies/#{tenancy.id}\"] input[name=\"tenancy[monthly_rent]\"]")
      )

      expect(rendered).to match(
        have_css("form[action=\"/tenancies/#{tenancy.id}\"] button[type=\"submit\"]")
      )

      expect(rendered).to_not match(/error/) # no errors on page
    end
  end

  context 'with validation errors' do
    it 'displays edit Tenancy form with error messages' do
      tenancy = create(:tenancy)
      tenancy.security_deposit = nil
      tenancy.validate
      assign(:tenancy, tenancy)

      render

      expect(rendered).to include 'Edit'

      expect(rendered).to match(
        have_css("form[action=\"/tenancies/#{tenancy.id}\"] select[name=\"tenancy[property_id]\"]")
      )

      expect(rendered).to have_field('tenancy_property_id', disabled: true)

      expect(rendered).to match(
        have_css("form[action=\"/tenancies/#{tenancy.id}\"] select[name=\"tenancy[tenant_ids][]\"]")
      )

      expect(rendered).to match(
        have_css("form[action=\"/tenancies/#{tenancy.id}\"] input[name=\"tenancy[start_date]\"]")
      )

      expect(rendered).to have_field('tenancy_start_date', disabled: true)

      expect(rendered).to match(
        have_css("form[action=\"/tenancies/#{tenancy.id}\"] input[name=\"tenancy[security_deposit]\"]")
      )

      expect(rendered).to match(
        have_css("form[action=\"/tenancies/#{tenancy.id}\"] input[name=\"tenancy[monthly_rent]\"]")
      )

      expect(rendered).to match(
        have_css("form[action=\"/tenancies/#{tenancy.id}\"] button[type=\"submit\"]")
      )

      expect(rendered).to match(/Is not a number/)
      expect(rendered).to match(/can&#39;t be blank/)
    end
  end
end
