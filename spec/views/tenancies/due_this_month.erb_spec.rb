# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'tenancies/due_this_month', type: :view do
  it 'displays all tenancies that are due to pay' do
    properties = create_list(:property, 3)

    tenancies = []
    tenancy1 = create(:tenancy, property_id: properties[0].id, start_date: Date.today)
    tenancies << tenancy1
    tenancy2 = create(:tenancy, property_id: properties[1].id, start_date: Date.today)
    tenancies << tenancy2

    assign(:tenancies, tenancies)

    render

    expect(rendered).to include "Rent due in #{ Date.today.strftime("%B") }"

    tenancies.each do |tenancy|
      expect(rendered).to include tenancy.property.property_name
      expect(rendered).to include tenancy.start_date.strftime("%d %b")
      expect(rendered).to include tenancy.monthly_rent.to_s
    end
  end
end
