# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'tenancies/index', type: :view do
  it 'displays all Tenancies' do
    tenancies = create_list(:tenancy, 2)
    assign(:tenancies, tenancies)

    render

    expect(rendered).to include 'Tenancies'

    tenancies.each do |tenancy|
      expect(rendered).to include tenancy.property.property_name
      expect(rendered).to include tenancy.start_date.to_s
      expect(rendered).to include tenancy.security_deposit.to_s
      expect(rendered).to include tenancy.monthly_rent.to_s

      tenancy.tenants.each do |tenant|
        expect(rendered).to include tenant.first_name
        expect(rendered).to include tenant.last_name
      end
    end
  end
end
