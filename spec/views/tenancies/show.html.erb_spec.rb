# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'tenancies/show', type: :view do
  it 'displays the Tenancy' do
    tenancy = create(:tenancy)
    assign(:tenancy, tenancy)

    render

    expect(rendered).to include 'Tenancy'

    expect(rendered).to include tenancy.property.property_name

    tenancy.tenants.each do |tenant|
    	expect(rendered).to include tenant.first_name
    	expect(rendered).to include tenant.last_name
    end

    expect(rendered).to include tenancy.start_date.to_s
    expect(rendered).to include tenancy.security_deposit.to_s
    expect(rendered).to include tenancy.monthly_rent.to_s
  end
end
