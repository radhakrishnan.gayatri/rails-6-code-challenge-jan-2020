# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TenanciesController, type: :controller do
  let(:current_user) { create(:user) }
  let(:session) { { user_id: current_user.id } }
  let(:params) { {} }

  describe 'GET #due_this_month' do
    subject { get :due_this_month, session: session }

    let!(:tenancy1) { create(:tenancy, start_date: Date.today) }
    let!(:tenancy2) { create(:tenancy, start_date: Date.today) }
    let!(:tenancy3) { create(:tenancy, start_date: Date.today - 1.month ) }

    it 'assigns @tenancies' do
      subject
      expect(assigns(:tenancies)).to contain_exactly(tenancy1, tenancy2)
      expect(assigns(:tenancies)).not_to include(tenancy3)
    end

    it 'responds with 200 OK' do
      subject
      expect(response).to have_http_status(:ok)
    end

    context 'when user is not logged in' do
      subject { get :index, params: params, session: {} }

      it 'returns http forbidden status' do
        subject
        expect(response).to have_http_status(:forbidden)
      end

      it 'renders error page' do
        subject
        expect(response).to render_template('errors/not_authorized')
      end
    end
  end
end
