# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TenanciesController, type: :controller do
  let(:current_user) { create(:user) }
  let(:session) { { user_id: current_user.id } }
  let(:params) { {} }

  describe 'PUT #update' do
    subject { put :update, params: params, session: session }

    let!(:tenancy) { create(:tenancy) }
    let(:params) { { id: tenancy.id } }

    context 'when valid tenancy param attributes' do
      let(:valid_tenancy_attributes) do
        {
          property_id: create(:property).id,
          start_date: Date.today,
          security_deposit: 2000.00,
          monthly_rent: 1000.00,
          tenant_ids: [create(:tenant).id]
        }
      end
      let(:params) { { id: tenancy.id, tenancy: valid_tenancy_attributes } }

      it 'assigns @tenancy' do
        subject
        expect(assigns(:tenancy)).to be_a Tenancy
      end

      it 'updates the Tenancy' do
        subject

        tenancy.reload
        expect(tenancy.property_id).to eq valid_tenancy_attributes[:property_id]
        expect(tenancy.start_date).to eq valid_tenancy_attributes[:start_date]
        expect(tenancy.security_deposit).to eq valid_tenancy_attributes[:security_deposit]
        expect(tenancy.monthly_rent).to eq valid_tenancy_attributes[:monthly_rent]
        expect(tenancy.tenant_ids).to eq valid_tenancy_attributes[:tenant_ids]
      end

      it 'responds with 302 Found' do
        subject
        expect(response).to have_http_status(:found)
      end

      it 'redirects to tenancies#show' do
        subject
        expect(response).to redirect_to Tenancy.last
      end

      it 'assigns flash success' do
        subject
        expect(flash[:success]).to eq 'Tenancy was successfully updated.'
      end
    end

    context 'when invalid tenancy param attributes' do
      let(:invalid_tenancy_attributes) do
        {
          property_id: nil,
          start_date: nil,
          security_deposit: nil,
          monthly_rent: nil,
          tenant_ids: []
        }
      end
      let(:params) { { id: tenancy.id, tenancy: invalid_tenancy_attributes } }

      it 'does not update the Tenancy' do
        expect { subject }.to_not(change { tenancy.reload.attributes })
      end

      it 'responds with unprocessable_entity' do
        subject
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'when user is not logged in' do
      subject { put :update, params: params, session: {} }

      it 'returns http forbidden status' do
        subject
        expect(response).to have_http_status(:forbidden)
      end

      it 'renders error page' do
        subject
        expect(response).to render_template('errors/not_authorized')
      end
    end
  end
end
