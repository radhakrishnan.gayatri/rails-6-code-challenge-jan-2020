# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TenanciesController, type: :controller do
  let(:current_user) { create(:user) }
  let(:session) { { user_id: current_user.id } }
  let(:params) { {} }

  describe 'GET #show' do
    subject { get :show, params: params, session: session }

    let!(:tenancy) { create(:tenancy) }
    let(:params) { { id: tenancy.id } }

    it 'assigns @tenancy' do
      subject
      expect(assigns(:tenancy)).to eq tenancy
    end

    it 'responds with 200 OK' do
      subject
      expect(response).to have_http_status(:ok)
    end

    context 'when user is not logged in' do
      subject { get :show, params: params, session: {} }

      it 'returns http forbidden status' do
        subject
        expect(response).to have_http_status(:forbidden)
      end

      it 'renders error page' do
        subject
        expect(response).to render_template('errors/not_authorized')
      end
    end
  end
end
