# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TenanciesController, type: :controller do
  let(:current_user) { create(:user) }
  let(:session) { { user_id: current_user.id } }
  let(:params) { {} }

  describe 'DELETE #destroy' do
    subject { delete :destroy, params: params, session: session }

    let(:property) { create(:property)}
    let(:tenant) { create(:tenant)}
    let!(:tenancy) { create(:tenancy, property_id: property.id)}
    let(:tenacies_tenant) { create(:tenancies_tenant, tenancy_id: tenancy.id, tenant_id: tenant.id) }

    let(:params) { { id: tenancy.id } }

    it 'destroys the Tenancy' do
      expect { subject }.to change(Tenancy, :count).by(-1)
      expect { tenancy.reload }.to raise_error ActiveRecord::RecordNotFound
    end

    context 'when user is not logged in' do
      subject { delete :destroy, params: params, session: {} }

      it 'returns http forbidden status' do
        subject
        expect(response).to have_http_status(:forbidden)
      end

      it 'renders error page' do
        subject
        expect(response).to render_template('errors/not_authorized')
      end
    end
  end
end
