# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PropertiesController, type: :controller do
  describe 'GET #advertised_properties' do
    subject { get :advertised_properties, session: nil }

    let!(:properties) { create_list(:property, 5) }
    let!(:tenancy1) { create(:tenancy, property_id: properties[0].id) }
    let!(:tenancy2) { create(:tenancy, property_id: properties[1].id) }

    it 'will not authenticate user' do
      subject
      expect(controller).not_to receive(:authenticate_user!)
    end

    it 'will return unrented properties' do
      subject
      expect(assigns(:properties)).to contain_exactly(properties[2], properties[3], properties[4])
      expect(assigns(:properties)).not_to include(properties[0], properties[1])
    end
  end
end