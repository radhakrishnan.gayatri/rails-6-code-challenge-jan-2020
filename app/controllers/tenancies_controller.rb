# frozen_string_literal: true

class TenanciesController < ApplicationController
  before_action :set_tenancy, only: %i[show edit update destroy]

  def index
    @tenancies = Tenancy.all
  end

  def due_this_month
    @tenancies = Tenancy.all.due_this_month
  end

  def show; end

  def new
    @tenancy = Tenancy.new
    if params[:property_id]
      @tenancy.property_id = params[:property_id]
    end
  end

  def edit; end

  def create
    @tenancy = Tenancy.create(tenancy_params)
    if @tenancy.persisted?
      flash[:success] = 'Tenancy was successfully created.'
      redirect_to @tenancy
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @tenancy.update(tenancy_params)
      flash[:success] = 'Tenancy was successfully updated.'
      redirect_to @tenancy
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @tenancy.destroy

    redirect_to tenancies_url, notice: 'Tenancy was successfully destroyed.'
  end

  private

  def set_tenancy
    @tenancy = Tenancy.find(params[:id])
  end

  def tenancy_params
    params.require(:tenancy).permit(
      :property_id,
      :start_date,
      :security_deposit,
      :monthly_rent,
      tenant_ids: []
    )
  end
end
