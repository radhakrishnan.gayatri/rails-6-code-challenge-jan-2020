class LandlordsController < ApplicationController
	before_action :set_landlord, only: %i[show edit update destroy]

	def index
    @landlords = Landlord.all
  end

  def show; end

  def new
    @landlord = Landlord.new
  end

  def edit; end

  def create
    @landlord = Landlord.new(landlord_params)
    if @landlord.save
      flash[:success] = 'Landlord was successfully created.'
      redirect_to @landlord
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @landlord.update(landlord_params)
      flash[:success] = 'Landlord was successfully updated.'
      redirect_to @landlord
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @landlord.destroy

    redirect_to landlords_url, notice: 'Landlord was successfully destroyed.'
  end

  private

  def set_landlord
    @landlord = Landlord.find(params[:id])
  end

  def landlord_params
    params.require(:landlord).permit(
      :first_name,
      :last_name,
      :email,
      property_ids: []
    )
  end
end