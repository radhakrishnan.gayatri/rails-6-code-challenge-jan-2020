# frozen_string_literal: true

class Property < ApplicationRecord
  PROPERTY_NAME_MAX_LENGTH = 100
  PROPERTY_ADDRESS_MAX_LENGTH = 500

  # rubocop:disable Layout/LineLength
  # rubocop:enable Layout/LineLength

  validates :property_name, presence: true
  validates :property_name, uniqueness: { case_sensitive: false }
  validates :property_name, length: { maximum: PROPERTY_NAME_MAX_LENGTH,
                                      if: :property_name }
  validates :property_address, presence: true
  validates :property_address, length: { maximum: PROPERTY_ADDRESS_MAX_LENGTH,
                                         if: :property_address }

  has_many :landlords_properties
  has_many :landlords, through: :landlords_properties, dependent: :destroy

  validate :validate_landlords

  has_one :tenancy, inverse_of: :property

  scope :not_rented, -> { joins('LEFT JOIN tenancies ON properties.id = tenancies.property_id')
    .where('tenancies.id IS NULL') }

  def validate_landlords
    errors.add(:landlords, "Pick at least one landlord") if landlords.size < 1
  end
end
