class Landlord < ApplicationRecord
  FIRST_NAME_MAX_LENGTH = 100
  LAST_NAME_MAX_LENGTH = 100
  EMAIL_MAX_LENGTH = 200

  # rubocop:disable Layout/LineLength
  EMAIL_REGEX = /\A([a-z0-9\+_\-\']+)(\.[a-z0-9\+_\-\']+)*@([a-z0-9\-]+\.)+[a-z]{2,6}\z/ix.freeze
  # rubocop:enable Layout/LineLength

  validates :first_name, presence: true
  validates :first_name, length: { maximum: FIRST_NAME_MAX_LENGTH,
                                  if: :first_name }

  validates :last_name, presence: true
  validates :last_name, length: { maximum: LAST_NAME_MAX_LENGTH,
                                  if: :last_name }

  validates :email, presence: true
  validates :email, uniqueness: { case_sensitive: false }
  validates :email, length: { maximum: EMAIL_MAX_LENGTH,
                              if: :email }
  validates :email, format: { with: EMAIL_REGEX },
                              if: -> { errors[:email].blank? }

  has_many :landlords_properties
  has_many :properties, through: :landlords_properties

  before_destroy :destroy_related_properties

  def destroy_related_properties
    properties_to_delete = properties
    co_owned_property_ids = co_owned_properties.pluck(:id)

    if co_owned_property_ids.any?
      # 1. Delete only association for the landlord to the property.
      landlords_properties.where(property_id: co_owned_property_ids).destroy_all

      # 2. Pick property where deleted landlord is the only one.
      properties_to_delete = properties_to_delete.where.not(id: co_owned_property_ids)
    end

    # Delete properties where deleted landlord is the only one.
    if properties_to_delete
      properties_to_delete.destroy_all
    end
  end

  def co_owned_properties
    properties.joins(:landlords).where.not(landlords: {id: self.id}).distinct
  end
end