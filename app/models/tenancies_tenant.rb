class TenanciesTenant < ApplicationRecord

  validates :tenancy, presence: true
  validates :tenant, presence: true

  validates_uniqueness_of :tenancy_id, scope: [:tenant_id]

  belongs_to :tenancy
  belongs_to :tenant
end