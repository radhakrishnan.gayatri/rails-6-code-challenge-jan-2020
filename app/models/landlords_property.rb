class LandlordsProperty < ApplicationRecord

  validates :landlord, presence: true
  validates :property, presence: true

  validates_uniqueness_of :property_id, scope: [:landlord_id]

  belongs_to :landlord
  belongs_to :property
end