# frozen_string_literal: true

class Tenancy < ApplicationRecord

  validates :property, presence: true
  validates :start_date, presence: true
  validates :security_deposit, numericality: { greater_than: 0.0}, presence: true
  validates :monthly_rent, numericality: { greater_than: 0.0}, presence: true
  validate :validate_tenants

  belongs_to :property, inverse_of: :tenancy

  has_many :tenancies_tenants
  has_many :tenants, through: :tenancies_tenants, dependent: :destroy

  scope :due_this_month, -> {where("cast(strftime('%m', start_date) as int) = ?", Date.today.month)}

  def validate_tenants
    errors.add(:tenants, "Pick at least one tenant") if tenants.size < 1
  end
end
